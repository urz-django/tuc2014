#!/usr/bin/env bash
set -e
wget https://www-user.tu-chemnitz.de/~klada/tuc2014/ -O tuc2014/templates/tuc2014/base_de.html
wget https://www-user.tu-chemnitz.de/~klada/tuc2014/index.php.en -O tuc2014/templates/tuc2014/base_en.html

sed -i 's/www.tu-chemnitz.de\/static\//static.tu-chemnitz.de\//' tuc2014/templates/tuc2014/base_*.html
#sed -i 's/www.tu-chemnitz.de\/tucal4\//static.tu-chemnitz.de\/tucal4\//' tuc2014/templates/tuc2014/base_*.html

echo
echo "Achtung!"
echo "Stylesheet muss manuell hinzugefügt werden:"
echo '<link rel="stylesheet" type="text/css" href="https://static.tu-chemnitz.de/jquery-ui/1.14/themes/smoothness/jquery-ui.min.css" />'
