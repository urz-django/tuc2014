# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings
from django.shortcuts import render
from django.utils import translation

# Check if crispy_forms is installed and set up correctly
if 'crispy_forms' in getattr(settings, 'INSTALLED_APPS', []):
    from .forms import ChangeLanguageForm
    HAVE_CRISPY = True
else:
    from ._forms import ChangeLanguageForm
    HAVE_CRISPY = False


def change_language(request, **kwargs):
    """
    This view can be mounted anywhere and will show a form for
    changing the website's language.
    """
    referer = request.META.get('HTTP_REFERER')
    if not referer:
        referer = ""
    cur_language = translation.get_language()
    form = ChangeLanguageForm(referer, initial={'language': cur_language})
    if HAVE_CRISPY:
        template = 'tuc2014/change_language.html'
    else:
        template = 'tuc2014/change_language_nocrispy.html'
    return render(request, template, {'form': form, 'current': cur_language})


def handle_403(request, *args, **kwargs):
    return render(request, 'tuc2014/403.html', status=403)


def handle_404(request, *args, **kwargs):
    return render(request, 'tuc2014/404.html', status=404)
