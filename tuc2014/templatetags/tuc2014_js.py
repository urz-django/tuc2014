# -*- coding: utf-8 -*-
from __future__ import unicode_literals
"""
Template-Tag library for JavaScript-related functions.

@author: klada
"""
from django import template
from django.template import loader, TemplateDoesNotExist
from django.utils.safestring import mark_safe
from django.utils.translation import get_language

register = template.Library()

@register.simple_tag
def datefield_locale_include():
    """
    Includes the datepicker locale definition for the current locale.
    
    @return: The necessary JavaScript code
    @rtype: string
    """
    lang = get_language()
    if lang == "en" or lang == 'en-US':
        return "datefield_datepicker_init = true;"
    
    # The datepicker requires uppercase variants, such as zh-CN, Django only uses lowercase variants
    lang = lang.lower() 
    if '-' in lang:
        base, variant = lang.split('-')
        variant = variant.upper()
    else:
        base = lang
        variant = lang.upper()
    
    try_template_names = (
        'tuc2014/js/jquery-datepicker-locale/datepicker-%s-%s.min.js' %(base, variant),
        'tuc2014/js/jquery-datepicker-locale/datepicker-%s.min.js' %base,
    )
    
    for name in try_template_names:
        try:
            content = loader.render_to_string(name)
        except TemplateDoesNotExist:
            pass
        else:
            return mark_safe(content + ' datefield_datepicker_init = true;')
    return ""


@register.simple_tag
def datefield_dateformat_for_field(field):
    """
    Returns the date format for JQueryUI's date picker which matches the field's expected input format.
    
    :param field: A DateField instance
    :type field: django.forms.fields.DateField
    :return: A dateFormat for JQueryUI's datepicker, which matches the field's date format
    :rtype: str
    """
    # The widget's format takes precedence. Usually this is not defined.
    if hasattr(field.widget, "format") and field.widget.format:
        date_format = field.widget.format
    else:
        try:
            # `input_formats` actually is a list. The first element in that list will be used by Django's form renderer
            # when rendering date objects (such as default values), so we are also using it for the date picker.
            date_format = field.input_formats[0]
        except KeyError:
            date_format = "%Y-%m-%d"
    # JqueryUI expects a different format date syntax, so let's convert the most common things :(
    date_format = date_format.replace("%Y", "yy")
    date_format = date_format.replace("%y", "y")
    date_format = date_format.replace("%d", "dd")
    date_format = date_format.replace("%m", "mm")
    return date_format
