��          \      �       �      �      �   B         C     L  k   S  �   �  B  R  !   �  #   �  X   �     4     <  x   E  �   �                                       Error 403: Access denied Error 404: Web page not found If you think you should have access, please contact the webmaster. Language Submit The author of this web site has restricted the access. That's why you are not allowed to use this web page. The website you have requested does not exist. If you have reached this website through a link, please inform the webmaster about the broken link. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Fehler 403: Zugriff nicht erlaubt Fehler 404: Webseite nicht gefunden Wenn Sie glauben, dass Sie berechtigt sein sollten, informieren Sie bitte den Webmaster. Sprache Absenden Der Autor dieses Webangebots hat den Zugriff eingeschränkt. Danach sind Sie nicht berechtigt, diese Webseite zu nutzen. Die von Ihnen aufgerufene Webseite existiert nicht. Wenn Sie diese Webseite über einen Link erreicht haben, informieren Sie bitte den Webmaster über den defekten Link. 