# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime
from django.conf import settings
from django.urls import reverse


def tucal(request):

    language_uri = getattr(settings, 'TUCAL_LANGUAGE_SELECT', '')
    if not language_uri:
        try:
            # Try to guess the URL
            language_uri = reverse('tuc2014:change_language')
        except:
            language_uri = ''

    today = datetime.date.today()

    haupttitel = getattr(settings, 'TUCAL_HAUPTTITEL', '')
    language_code = getattr(request, 'LANGUAGE_CODE', settings.LANGUAGE_CODE).lower()

    if language_code == 'de' or language_code.startswith('de-'):
        tucal_base_template = 'tuc2014/base_de.html'
    else:
        tucal_base_template = 'tuc2014/base_en.html'

    return {
        'tucal_autor': getattr(settings, 'TUCAL_AUTOR', 'Webmaster'),
        'tucal_haupttitel': haupttitel,
        'tucal_impressum': getattr(settings, 'TUCAL_IMPRESSUM', '//www.tu-chemnitz.de/tu/impressum.html'),
        'tucal_keywords': getattr(settings, 'TUCAL_KEYWORDS', ''),
        'tucal_base_template': tucal_base_template,
        'tucal_language_uri': language_uri,
        'tucal_last_modified': today,
        'tucal_logos': getattr(settings, 'TUCAL_LOGOS', []),
        'tucal_titel': getattr(settings, 'TUCAL_TITEL', haupttitel),
        'tucal_url': request.build_absolute_uri(),
        'tucal_year': "{}".format(today.year),
    }


def press(request):
    return {'display_press': False}
