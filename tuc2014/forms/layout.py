"""
Fields and Layout classes which can be used with crispy-forms.

@author: klada
@see: http://django-crispy-forms.readthedocs.org/en/latest/layouts.html
"""
import json
from distutils.version import StrictVersion
from crispy_forms import __version__ as CRISPY_VERSION
from crispy_forms.layout import Field
from django.utils.safestring import mark_safe

##
# crispy_forms 2.0 changes the signature of the render() method. Since we need
# to support older crispy versions we have to add a compat layer here.
#
# https://github.com/django-crispy-forms/django-crispy-forms/releases/tag/2.0
##
class _RenderHelperMixin:
    def render(self, form, context, *args, **kwargs):
        context['datepicker_options'] = mark_safe(json.dumps(self.datepicker_options))
        return super().render(form, context, *args, **kwargs)


class _LegacyRenderHelperMixin:
    def render(self, form, form_style, context, *args, **kwargs):
        context['datepicker_options'] = mark_safe(json.dumps(self.datepicker_options))
        return super().render(form, form_style, context, *args, **kwargs)


if StrictVersion(CRISPY_VERSION) < StrictVersion("2.0"):
    _helper_mixin = _LegacyRenderHelperMixin
else:
    _helper_mixin = _RenderHelperMixin


class DateField(_helper_mixin, Field):
    """
    Crispy forms layout field which adds a jquery-ui datepicker
    to the widget.
    
    The datepicker is fully customizable through keyword arguments, but
    the arguments need to be prefixed with `datepicker_`.
    
    Example:
    
        DateField('my_date_field', datepicker_maxDate='+1M', datepicker_minDate='0') 
    """
    template = 'tuc2014/forms/layout/datefield.html'
    
    def __init__(self, *args, **kwargs):
        # get datepicker arguments from kwargs
        self.datepicker_options = {}
        kwargs_keys = kwargs.keys()
        # in python3 kwargs_keys is an interator which raises an exception because 
        # we change the underlying dict. -> converting to list decouples the two
        for i in list(kwargs_keys):
            if i.startswith('datepicker_'):
                arg = kwargs.pop(i)
                #strip the datepicker_prefix
                attr = i.split('datepicker_')[1]
                self.datepicker_options[attr] = arg
        super(DateField, self).__init__(*args, **kwargs)

