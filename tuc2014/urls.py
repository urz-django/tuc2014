# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.urls import include, re_path

from .views import change_language


app_name = 'tuc2014'
urlpatterns = [
    re_path(r'^i18n/', include(('django.conf.urls.i18n', 'djangoi18n'), namespace='djangoi18n')),
    re_path(r'^$', change_language, name='change_language')
]
