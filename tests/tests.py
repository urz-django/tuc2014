# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.test import Client, TestCase
from django.urls import reverse


class Tuc2014TestCase(TestCase):
    CONTENT_BLOCK = '<div id="id_tests_block_content">CONTENT_BLOCK</div>'

    def setUp(self):
        self.client = Client()

    def test_index(self):
        response = self.client.get('/')
        self.assertEquals(response.status_code, 200)

        self.assertTrue(self.CONTENT_BLOCK in str(response.content, "utf-8"))

    def test_admin(self):
        user = User.objects.create_superuser(username="test", email="", password="test")
        user.set_password("test")
        user.save()
        self.client.login(username="test", password="test")
        response = self.client.get('/admin/')
        self.assertEquals(response.status_code, 200)

    def test_urlconf_i18n(self):
        reverse('djangoi18n:set_language', 'tuc2014.urls')

    def test_urlconf_tuc2014(self):
        reverse('change_language', 'tuc2014.urls')
