from django.urls import re_path
from django.contrib import admin
from .views import index

urlpatterns = [
    re_path(r'^$', index),
    re_path('admin/', admin.site.urls),
]


handler403 = 'tuc2014.views.handle_403'
handler404 = 'tuc2014.views.handle_404'
