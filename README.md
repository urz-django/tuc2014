django-tuc2014
==============

Diese Django-App ermöglicht die Nutzung des Corporate Designs der TU Chemnitz mit dem Django-Template-System.
Dazu wird sowohl ein Basis-Template für eigene Django-Seiten, als auch ein angepasstes Template für Inhalte der
Admin-Seiten bereitgestellt. 

Installation
------------

Zur Installation mit pip kann folgendes Kommando verwendet werden:

```
pip install -e git+https://gitlab.hrz.tu-chemnitz.de/urz-django/tuc2014.git#egg=django-tuc2014
```

Generell wird die Installation in einer virtuellen Python-Umgebung (*virtualenv*) empfohlen.
Dazu sollte das Paket in die `requirements.txt` des jeweiligen Django-Projektes aufgenommen werden.


Django Konfiguration
--------------------

In den Projekteinstellungen (`settings.py`) müssen folgende Anpassungen vorgenommen werden:

1. Hinzufügen der Anwendung zur Liste der`INSTALLED_APPS`. Normalerweise sollte die Anwendung als erstes Element aufgeführt werden:
   ```python 
    INSTALLED_APPS = (
      'tuc2014',
      # ...
    )
   ```

2. Aktivierung des ``tucal``-Template-Context-Processors in den `TEMPLATES`-Einstellungen:
   ```python
    TEMPLATES = [
        {
            'BACKEND': 'django.template.backends.django.DjangoTemplates',
            'DIRS': [],
            'APP_DIRS': True,
            'OPTIONS': {
                'context_processors': [
                    # ....
                    # zusätzlich:
                    'tuc2014.context_processors.tucal',
                ],
            },
        },
    ]
   ``` 

3. Folgende Einstellungen **können** zusätzlich in den Django-Settings gesetzt werden:
   * `TUCAL_AUTOR`: Der Autor der Webseite. Standardwert: *Webmaster*
   * `TUCAL_HAUPTTITEL`: Der Kopftitel der Webseite. Standardwert: *leer*
   * `TUCAL_TITEL`: Der eigentliche Titel der Webseite. Standardwert: *leer*
   * `TUCAL_IMPRESSUM`: Ein Link zum Impressum der Seite. Standardwert: `//www.tu-chemnitz.de/tu/impressum.html`
   * `TUCAL_LANGUAGE_SELECT`: Link zum View, welches die Sprache wählen kann. Standardwert: *keiner*
   * `TUCAL_LOGOS`: Eine Liste von Logos, die unten links auf der Seite angezeigt werden. Beispiel:
     ```python
     TUCAL_LOGOS = [
         {'imgurl': '//www.tu-chemnitz.de/pfad/zu/logo.jpg', 'alt': 'Alternativer Logotext', 'title': 'Logo-Titel'},
     ]
     ```

4. **(Optional)**: Wenn das Projekt mehrsprachig verfügbar sein soll, müssen folgende Einstellungen vorgenommen werden:
   * In der `urls.py`:
     ```python
     # urls.py Auszug:
     from django.urls import include, path
     
     urlpatterns = [
         path('language/', include(('tuc2014.urls', 'tuc2014'), namespace='tuc2014')),
         # ... weitere Patterns
     ]
     ```
   * In den `settings.py` muss in `MIDDLEWARE` die `LocaleMiddleware` enthalten sein:
     ```python
     MIDDLEWARE = [
         # ...
         'django.middleware.locale.LocaleMiddleware',
     ]
     ```


Nutzung in Templates
--------------------

Eigene Templates können das Corporate Design durch Erben vom Basis-Template ``tuc2014/base.html`` verwenden. Dieses Basis-Template definiert einige Template-Blöcke, die überschrieben werden können bzw. müssen:

- Block **content**:

  Dieser Block ist wohl der wichtigste - er enthält den eigentlichen Seiteninhalt.
  
- Block **pagemenu**:

  Das Menü auf der linken Seite. Das Menü muss manuell angelegt und verwaltet werden, ``django-tuc2014`` macht hier keine weiteren Vorgaben zum Aufbau. Es wird jedoch empfohlen, sich an die offiziellen Design-Richtlinien zu halten.
  
  Die Einträge im Menü selbst sind Listenelemente mit einem Link. Second-Level-Menüs werden durch eine ungeordnete Liste (``<ul>``) angelegt.
  
- Block **breadcrumbs**:

  Definiert die Brotkrumen-Navigation unter dem Seitenlogo. Diese besteht ähnlich wie das Menü auf der linken Seite aus Listenelementen, die selbst definiert werden müssen.
  
  Um Wiederholungen zu vermeiden, sollten verschachtelte Seiten die Brotkrumen ihrer übergeordneten Seiten erben. Verdeutlicht wird das in folgendem Beispiel:
  
  ```html
   {% block breadcrumbs %}
    {{block.super}}
    <li><a href="/foo/">Foo-Seite</a></li>
    <li><a href="/foo/bar/">Bar-Seite</a></li>
   {% endblock %} 
   ```

- Block **extrahead**:
   
  Kann verwendet werden, um zusätzliche Informationen im HTML-Kopf zu definieren. Eigene Scripte und CSS-Dateien können hier ebenfalls eingebunden werden.

- Block **tuc_refmenu**:

  Hier können optional Verweise wie folgt untergebracht werden:

  - Entweder direkt über die Template-Kontext-Variable ``tucal_verweise``:
    ```python
    context['tucal_verweise'] = [
        {'name': 'Demo-Verweis', 'url': '/demo/verweis/'},
        {'name': 'Externer-Verweis', 'url': 'http://www.example.org/', 'extern': True},
        {'name': 'Noch ein Verweis', 'url': '/foo/', 'title': 'Beschreibung', 'target': '_blank'},
    ]
    ```

  - Sollen die Links global im ganzen Projekt hinzugefügt werden, bietet sich dafür die Definition
    eines *Context Processors* an. Details dazu gibt es in der Django-Doku.

  - Will man die Kontextvariable nicht in jedem View separat übergeben oder einen *Context Processor* definieren,
    dann kann man auch den Template-Block `tuc_refmenu` überschreiben:

    ```html
    {% block tuc_refmenu %}
        <div id="tucal-refmenu">
            <ol>
                <li class="extern"><a href="https://example.org">Beispiel</a></li>
            </ol>
        </div>
    {% endblock tuc_refmenu %}
    ```
    
    Zu beachten ist hier aber, dass sich die gesamte HTML-Struktur, einschließlich IDs und CSS-Klassen jederzeit ändern
    kann. Daher wird allgemein empfohlen, dieses Menü direkt über den Template-Kontext zu definieren.


Formulare mit django-crispy-forms
---------------------------------
 
Für das Rendering von Formularen kann **django-crispy-forms** mit den *bootstrap3*-Template-Pack
verwendet werden. Die Formulare passen sich dann wunderbar in den Rest der Webseite ein
und sind dank Bootstrap auch *responsive*.

Für den komfortablen Umgang mit **django-crispy-forms** bietet dieses Paket einige hilfreiche Basisklassen,
die sich im Modul `tuc2014.forms` befinden.

### Konfiguration

Nach der Installation von `django-crispy-forms` **und** `crispy-bootstrap3` müssen folgende
Einstellungen vorgenommen werden:

```python

INSTALLED_APPS = [
    // ...
    'crispy_forms',
    'crispy_bootstrap3',
]

# Wichtig: Template-Pack setzen. Standard: 'bootstrap'
CRISPY_TEMPLATE_PACK = 'bootstrap3'
```

### Definition von Layouts

Wenn die Klassen `TucForm` bzw. `TucModelForm` aus dem Modul `tuc2014.forms` als Basisklasse für Formulare
verwendet werden, kann das Layout dabei für einfache Formulare gänzlich automatisch generiert werden:

 ```python
from tuc2014.forms import TucModelForm

class DemoModelForm(TucModelForm):
    crispy_default_layout = True
     
    class Meta:
        model = DemoModel
        fields = ('field_a', 'field_b')
 ``` 

Genügt das Standard-Layout nicht, kann es direkt innerhalb der Definition der Formularklasse definiert werden:
 
 ```python
from crispy_forms.bootstrap import FormActions
from crispy_forms.layout import Div, Layout, Submit, HTML, Fieldset
from tuc2014.forms import TucModelForm

class DemoModelForm(TucModelForm):
    layout = Layout(
        Fieldset(
            'Fieldset Label',
            'field_a', 'field_b'
        ),
        FormActions(
            Submit('submit', 'Go!')
        )
    )
     
    class Meta:
        model = DemoModel
        fields = ('field_a', 'field_b')
        
```

Das Rendering des Formulars im Template kann dann der `crispy`-Tag übernehmen:

```html
{% extends "tuc2014/base.html" %}
{% load crispy_forms_tags static %}

{% block extrahead %}
<link rel="stylesheet" type="text/css" href="{% static "tuc2014/css/forms.css" %}" />
{% endblock extrahead %}

{% block content %}
 {% crispy form %}
{% endblock content %}
```

Idealerweise sollte auch das CSS wie oben angegeben mit geladen werden.

